package com.example.reveladorcafe.setsuna;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.*;
import android.os.Handler;

public class GameActivityMulti extends Activity {

    /**
     * ボタンリスト
     */
    private List<Button> _btnList;

    /**
     * スコアリスト
     */
    private Integer[] _scoreArray = new Integer[]{0,0,0,0,0,0};

    /**
     *スタートフラグ実行用ハンドラ
     */
    private final Handler handler = new Handler();

    /**
     *最初に正しくタッチした際にTrue
     */
    private boolean _firstTouchFlg;

    /**
     * スタートフラグ変更用タスク
     */
    private final Runnable startFlgChangeTask = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), "start", Toast.LENGTH_SHORT).show();
            Const.START_FLAG = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_multi);

        //スタートフラグ初期化
        Const.START_FLAG = false;

        //ボタンリスト生成
        _btnList = new ArrayList<Button>();
        _btnList.add((Button)findViewById(R.id.button1));
        _btnList.add((Button)findViewById(R.id.button2));
        _btnList.add((Button)findViewById(R.id.button3));
        _btnList.add((Button)findViewById(R.id.button4));
        _btnList.add((Button)findViewById(R.id.button5));
        _btnList.add((Button)findViewById(R.id.button6));

        handler.postDelayed(startFlgChangeTask, 5000);

    }

    /*
    * ボタンクリック
    * */
    public void PublicOnClickListener(View view){

        //すでに正しくタッチしたものが存在する場合は以降の処理は行わない
        if(_firstTouchFlg)
        {
            return;
        }
        Button btn = (Button)view;
        String tempBtnTxt = btn.getText().toString();
//        //デバック用スタートフラグON
//        if(btn.getText().equals("Player6"))
//        {
//           Const.START_FLAG = true;
//            return;
//        }

        //スタートフラグが経ってない状態
        if(Const.START_FLAG == false)
        {
            //フライング
            //Toast.makeText(this,tempBtnTxt+"フライング",Toast.LENGTH_SHORT).show();

            //フライングの為ボタン無効
            btn.setEnabled(false);

            //すべてのボタンが無効になっていた場合はダイアログ表示
            if(this.GetAllBtnEnable() == false)
            {
                //ラウンド終了ダイアログ表示
                this.PushRoundFinishDialog("無効ゲーム");

                //再スタート
                RestartGame();
            }
            return;
        }

        //最初に正しくタップしたものが現れたためフラグON
        _firstTouchFlg = true;

        //スコア計算
        if(this.CalcScore(tempBtnTxt))
        {
            //ゲーム終了
            this.PushFinishDialog("☆★"+tempBtnTxt+"の勝ち!!!!!!☆★");


        }else
        {
            //ラウンド終了ダイアログ表示
            this.PushRoundFinishDialog(tempBtnTxt);

        }
    }

    /**
     * 引数で渡されたプレイヤー名のスコアを+1する
     * ノルマに達した場合はTrueを返却
     * @param player
     * @return
     */
    private boolean CalcScore(String player)
    {
        int tempScore = 0;
        boolean judge = false;

        if (player.indexOf("1") != -1) {
            // 部分一致
            _scoreArray[0] +=1;
            tempScore = _scoreArray[0];
        }
        else if (player.indexOf("2") != -1) {
            // 部分一致
            _scoreArray[1] +=1;
            tempScore = _scoreArray[1];
        }
        else if (player.indexOf("3") != -1) {
            // 部分一致
            _scoreArray[2] +=1;
            tempScore = _scoreArray[2];
        }
        else if (player.indexOf("4") != -1) {
            // 部分一致
            _scoreArray[3] +=1;
            tempScore = _scoreArray[3];
        }
        else if (player.indexOf("5") != -1) {
            // 部分一致
            _scoreArray[4] +=1;
            tempScore = _scoreArray[4];
        }
        else if (player.indexOf("6") != -1) {
            // 部分一致
            _scoreArray[5] +=1;
            tempScore = _scoreArray[5];
        }

        //スコアがノルマに達成した場合はTrueを返却
        if(tempScore >= Const.ROUND_FINISH_SCORE){
            judge = true;
        }

        return judge;

    }
    /**
     * すべてのボタンの使用可/不可を変更する
     * @param enableFlg
     */
    private void SetAllBtnEnable(boolean enableFlg)
    {

        for(Button tmpBtn: _btnList)
        {
            tmpBtn.setEnabled(enableFlg);
        }
    }

    /**
     * すべてのボタンが使用不可の場合:False 一つでも有効なボタンが存在する場合:True
     */
    private boolean GetAllBtnEnable()
    {
        //
        for(Button tmpBtn : _btnList)
        {
            if(tmpBtn.isEnabled() == true){
                return true;
            }else
            {
                continue;
            }
        }
        return false;
    }

    /**
     * 再スタート処理
     */
    private void RestartGame()
    {
        //ボタン表示初期化
        this.initButton();

        //フラグオフ
        Const.START_FLAG = false;

        //タイマー再スタート
        handler.postDelayed(startFlgChangeTask, 5000);

        //最初にタッチしたものフラグ初期化
        _firstTouchFlg = false;
    }


    /**
     * ラウンド終了ダイアログ表示
     * @param playerName
     */
    private void PushRoundFinishDialog(String playerName)
    {
        // 確認ダイアログの作成
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(GameActivityMulti.this).setCancelable(false);
        alertDialog.setTitle("INFO");
        alertDialog.setMessage(playerName+"Win");
        alertDialog.setPositiveButton("NextGame", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //再スタート
                GameActivityMulti.this.RestartGame();
            }
        });
        alertDialog.create().show();

    }

    /**
     * ゲーム終了ダイアログ表示
     * @param txt
     */
    private void PushFinishDialog(String txt)
    {
        // 確認ダイアログの作成
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(GameActivityMulti.this).setCancelable(false);
        alertDialog.setTitle("INFO");
        alertDialog.setMessage(txt);
        alertDialog.setPositiveButton("NextGame", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //ゲーム画面終了
                GameActivityMulti.super.finish();
            }
        });
        alertDialog.create().show();

    }


    /**
     * ボタン表示初期化
     *
     */
    private void initButton()
    {
        //ボタン表示の初期化
        this.SetAllBtnEnable(true);

        //ボタン色の初期化(透明)
//        this.SetAllBtnColor(Color.TRANSPARENT);
    }

    /**
     * 全てのボタンの表示色を変更する
     * @param color
     */
    private void SetAllBtnColor(int color)
    {
        for(Button tmpBtn: _btnList)
        {
            tmpBtn.setBackgroundColor(color);
        }

    }
}
