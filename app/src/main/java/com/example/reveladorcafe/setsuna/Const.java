package com.example.reveladorcafe.setsuna;

/**
 * Created by kensuke on 2016/12/17.
 */
/*
* 定数定義クラス
* */
public final  class Const {
    public static final String KEY_PLAYER_COUNT = "playerCount";

    /*
    * スタートフラグ
    * */
    public static Boolean START_FLAG;

    /**
     * ラウンドスコア
     */
    public static final Integer ROUND_FINISH_SCORE = 2;

}
